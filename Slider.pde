/*
  (c) 2013 code by Abraham Pazos - www.hamoid.com
  Concept and installation by Déborah Borque - www.deborahborque.com
*/

class Slider {
  Item[] items;
  int currentItem;

  boolean keyPressedBefore;

  final public static int RIGHT = 0;
  final public static int UP = 1;
  final public static int DOWN = 2;

  Slider() {
    items = new Item[0];
    currentItem = -1;
    keyPressedBefore = false;
  }
  void add(Object container, String varName, String description, float min, float curr, float max, int orientation) {
    Item i = new Item(container, varName, description, min, curr, max, orientation);
    items = (Item[]) append(items, i);
  }
  void draw() {
    if (keyPressed) {
      if (!keyPressedBefore && key == ' ') {
        this.currentItem++;
        if (this.currentItem >= this.items.length) {
          this.currentItem = -1;
        }
        this.keyPressedBefore = true;
        this.save();
      }
    } 
    else {
      this.keyPressedBefore = false;
    } 
    if (this.currentItem >= 0) {
      Item i = this.items[this.currentItem];
      if (mousePressed)
        switch(i.orientation) {
        case Slider.UP:
          i.curr = map(mouseY, 0, height, i.max, i.min);
          break;
        case Slider.DOWN:
          i.curr = map(mouseY, 0, height, i.min, i.max);
          break;
        case Slider.RIGHT:
          i.curr = map(mouseX, 0, width, i.min, i.max);
          break;
        }

      fill(0);
      shadowText(i.description + ": " + int(i.curr), 100, 100);

      float x, y;
      stroke(#FF0000, 128);
      strokeWeight(4);
      switch(i.orientation) {
      case Slider.UP:
        y = map(i.curr, i.max, i.min, 0, height);
        line(0, y, width, y);
        break;
      case Slider.DOWN:
        y = map(i.curr, i.min, i.max, 0, height);
        line(0, y, width, y);
        break;
      case Slider.RIGHT:
        x = map(i.curr, i.min, i.max, 0, width);
        line(x, 0, x, height);
        break;
      }
      strokeWeight(1);
      stroke(0);

      i.setVar();
    }
  }
  void save() {
    JSONObject config = new JSONObject();

    for (int j=0; j<items.length; j++) {
      Item i = this.items[j];
      config.setInt(i.varName, int(i.curr));
    }    
    saveJSONObject(config, "config.json");
  }
  void load() {
    JSONObject config = loadJSONObject("config.json");
    for (int j=0; j<items.length; j++) {
      Item i = this.items[j];
      try {
        i.curr = config.getInt(i.varName);
        i.setVar();
      } 
      catch(Exception e) {
        println(i.varName + " not found in config.");
      }
    }
  }
}
class Item {
  Object container;
  String varName;
  String description;
  float min, curr, max;
  int orientation;
  Item(Object container, String varName, String description, float min, float curr, float max, int orientation) {
    this.container = container;
    this.varName = varName;
    this.description = description;
    this.min = min;
    this.curr = curr;
    this.max = max;
    this.orientation = orientation;
  }
  void setVar() {
    try {
      this.container.getClass().getDeclaredField(this.varName).setInt(this.container, int(this.curr));
    } 
    catch(NoSuchFieldException e) {
      println(e);
    } 
    catch(IllegalAccessException e) {
      println(e);
    }
  }
}

