/*
  (c) 2013 code by Abraham Pazos - www.hamoid.com
  Concept and installation by Déborah Borque - www.deborahborque.com
*/
import processing.video.*;

Capture video;
//Movie video;
DrawEngine drawEngine;
BlobEngine blobEngine;
Slider slider;

boolean calibrated = false;
boolean initialized = false;
boolean shiftDown = false;

void setup() {
  size(1280, 800);
  colorMode(HSB);
  frameRate(30);
  //noSmooth();
  startCamera();
  drawEngine = new DrawEngine();
  blobEngine = new BlobEngine();  
  slider = new Slider();
}
void startCamera() {
  String[] cameras = Capture.list();

  if (cameras.length == 0) {
    print("No se encontró ninguna cámara!");
    exit();
  } else if(cameras.length == 1) {
    println("Usando la única cámara detectada");
    video = new Capture(this, 640, 360, 30);
    video.start();
  } else {
    println("Cámaras detectadas:");
    for (int i = 0; i < cameras.length; i++) {
      String name = cameras[i];
      println(i, name);
      if(name.indexOf("FaceTime") < 0 && video == null) {
        println("  Activando esta cámara.");
        video = new Capture(this, 640, 360, name, 30);
        video.start();
        break;
      }
    }
  }
    
  //video = new Movie(this, "/home/funpro/Desktop/R/media/BEST.MP4");
  //video.loop();
  //video.speed(1);
  //video.volume(0);
}
void draw() {
  if (!video.available())
    return;
  video.read();
  video.loadPixels();
  
  if (calibrated) {
    blobEngine.find();
    blobEngine.match();
    blobEngine.addUnmatched();
    drawEngine.draw(shiftDown);
  } else {
    if (!initialized) {
      drawEngine.init(slider);
      blobEngine.init(slider);
      slider.load();
      initialized = true;
      println("Initialized");
    }
    drawEngine.debug();  
  }
  slider.draw();
}
void keyPressed() {
  //if(key == 'g') {
  //  video.jump(map(mouseX, 0, width, 0, video.duration()));
  //}
  if(key == CODED && keyCode == SHIFT) {
      shiftDown = true;
  }
  if(key == ENTER || key == RETURN) {
    if(shiftDown) {
      println("Calibrated");
      calibrated = true;
    } else {
      blobEngine.calibrate();
    }
  }
}
void keyReleased() {
  if(key == CODED && keyCode == SHIFT) {
      shiftDown = false;
  }  
}

