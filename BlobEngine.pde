/*
  (c) 2013 code by Abraham Pazos - www.hamoid.com
  Concept and installation by Déborah Borque - www.deborahborque.com
*/

class BlobEngine {
  final static private int MAXBLOBS = 60;
  final static private int ARRLEN = 500;
  final static private int BLOBPIXELTHRESHOLD = 20;
  final static private int MATCH_SIMILARITY_THRESHOLD = 100;
  final static private float LERP_AMOUNT = 0.3; 
  
  Blob[] mainArr;
  Blob[] foundArr;
  int count;
  int foundCount;
  int videoPixels;
  int w, h;

  int minSat, minBri, maxHueDist;
  int marginLeft, marginRight, marginTop, marginBottom;

  BlobEngine() {
    this.mainArr = new Blob[MAXBLOBS];
    this.foundArr = new Blob[MAXBLOBS];
    this.count = 0;
    this.foundCount = 0;
    this.videoPixels = 0;
  }
  
  void init(Slider s) {
    this.w = video.width;
    this.h = video.height;
    this.videoPixels = this.w * this.h;
    s.add(this, "marginLeft", "Margen izquierdo", 0, 25, this.w, Slider.RIGHT);
    s.add(this, "marginRight", "Margen derecho", 0, this.w-25, this.w, Slider.RIGHT);
    s.add(this, "marginTop", "Margen superior", 0, 25, this.h, Slider.DOWN);
    s.add(this, "marginBottom", "Margen inferior", 0, this.h-25, this.h, Slider.DOWN);
    s.add(this, "minSat", "Cuan saturadas deben ser las formas", 0, 80, 255, Slider.UP);
    s.add(this, "minBri", "Cuan brillantes deben ser las formas", 0, 40, 255, Slider.UP);
    s.add(this, "maxHueDist", "Cuanta variación de tono se acepta en una forma", 0, 25, 100, Slider.UP);
    for(int i=0; i<MAXBLOBS; i++) {
      this.foundArr[i] = new Blob();
    }
  }

  void calibrate() {
    this.find();
    this.mainArr = this.foundArr.clone();
    this.count = this.foundCount;
  }

  void find() {
    this.foundCount = 0;
    boolean[] checked = new boolean[videoPixels];

    for (int y=this.marginTop; y < this.marginBottom; y+=8) {
      for (int x=this.marginLeft; x < this.marginRight; x+=8) {
        int i = x + y * this.w;
        if (checked[i])
          continue;
        int[] pixelsToCheck = new int[ARRLEN];
        int nextPixel = 0;
        int pendingPixels = 0;
        int x0 = i % this.w;
        int y0 = i / this.w;
        int x1 = x0;
        int y1 = y0;
        int thue = int(hue(video.pixels[i]));

        pixelsToCheck[nextPixel % ARRLEN] = i;
        pendingPixels++;
        while (pendingPixels > 0) {
          pendingPixels--;
          final int currentPixel = pixelsToCheck[nextPixel++ % ARRLEN];
          final int dn = currentPixel+this.w*2;
          final int up = currentPixel-this.w*2;
          final int TL = up-1;
          final int BR = dn+1;
          if (TL < 0 || BR >= videoPixels) 
            continue;
          final int BL = dn-1;
          final int TR = up+1;
          final int le = (currentPixel-1) % this.w;
          final int ri = (currentPixel+1) % this.w;
          if (!checked[BR]) {
            checked[BR] = true;
            if (isSimilarBrightPixel(BR, thue)) {
              pixelsToCheck[(nextPixel+pendingPixels++) % ARRLEN] = BR;
              x1 = max(x1, ri);
              y1 = max(y1, dn / this.w);
            }
          }
          if (!checked[BL]) {
            checked[BL] = true;
            if (isSimilarBrightPixel(BL, thue)) {
              pixelsToCheck[(nextPixel+pendingPixels++) % ARRLEN] = BL;
              x0 = min(x0, le);
              y1 = max(y1, dn / this.w);
            }
          }
          if (!checked[TR]) {
            checked[TR] = true;
            if (isSimilarBrightPixel(TR, thue)) {
              pixelsToCheck[(nextPixel+pendingPixels++) % ARRLEN] = TR;
              x1 = max(x1, ri);
              y0 = min(y0, up / this.w);
            }
          }
          if (!checked[TL]) {
            checked[TL] = true;
            if (isSimilarBrightPixel(TL, thue)) {
              pixelsToCheck[(nextPixel+pendingPixels++) % ARRLEN] = TL;
              x0 = min(x0, le);
              y0 = min(y0, up / this.w);
            }
          }
        }

        // pixel count threshold for something to be considered a blob
        if (nextPixel > BLOBPIXELTHRESHOLD) {
          if (this.foundCount < MAXBLOBS) {
            this.foundArr[this.foundCount++].init((x1+x0)/2, (y1+y0)/2, nextPixel, thue);
          } 
          else {
            println("Too many blobs! " + (x1+x0)/2 + ", " + (y1+y0)/2);
          }
        }
      }
    }
  }

  boolean isSimilarBrightPixel(int i, int thue) {
    color c = video.pixels[i];
    int chue = (int)hue(c);
    return 
      saturation(c) > this.minSat && 
      brightness(c) > this.minBri && 
      abs((thue<128?thue+256:thue)-(chue<128?chue+256:chue)) < this.maxHueDist;
  }

  void match() {
    // for each original blob...
    for (int i=0; i<this.count; i++) {
      Blob old = mainArr[i];
      if (old.free)
        continue;
      int bestScore = 9999;
      int bestID = -1;
      // ...find closest found blob
      for (int j=0; j<this.foundCount; j++) {
        Blob nu = this.foundArr[j];
        if (nu.taken)
          continue;
        // calculate a score
        int hueDiff = abs((old.hue<128?old.hue+256:old.hue)-(nu.hue<128?nu.hue+256:nu.hue)) / 4;
        int xDist = int(abs(old.x-nu.x)) / 2;
        int yDist = int(abs(old.y-nu.y)) * 2; // la distancia vertical afecta más, pq no debería cambiar
        int sizeDiff = int(abs(old.size - nu.size));
        // if near, it's our candidate
        int score = hueDiff + xDist + yDist + sizeDiff;
        if (score < bestScore) {
          bestID = j;
          bestScore = score;
        }
      }
      // if something is close enough
      if (bestScore < MATCH_SIMILARITY_THRESHOLD) {
        Blob nu = this.foundArr[bestID];
        old.x = lerp(old.x, nu.x, LERP_AMOUNT);
        old.y = lerp(old.y, nu.y, LERP_AMOUNT);
        old.dx = nu.x - old.x;
        old.hue = int(lerp(old.hue, nu.hue, LERP_AMOUNT));
        old.size = lerp(old.size, nu.size, LERP_AMOUNT);
        old.alpha = min(200, old.alpha+10);
        old.free = false;
        nu.taken = true;
      } 
      else {
        // nothing close enough, fade out
        old.alpha -= 10;
        old.x += old.dx;
        if (old.alpha < 0) {
          old.alpha = 0;
          old.free = true;
        }
      }
      if(!old.free) {
        old.angle += old.dangle;
        old.dangle = lerp(old.dangle, old.dx/120.0, LERP_AMOUNT);
      }
    }
  }

  void addUnmatched() {
    // add unassigned found new blobs to the
    // original blob array
    int k=0;
    for (int j=0; j<this.foundCount; j++) {
      Blob nu = this.foundArr[j];
      if (!nu.taken) {
        while (k < MAXBLOBS && mainArr[k] != null && !mainArr[k].free) 
          k++;

        int targetID;
        if (k < MAXBLOBS)
          targetID = k;
        else
          targetID = this.count++;

        // reuse free spot
        try {
          if(targetID < MAXBLOBS)
            mainArr[targetID] = (Blob)nu.clone();
        } 
        catch(CloneNotSupportedException e) {
          println(e);
        }
      }
    }
  }
  void debug(float scaleX, float scaleY) {
    // draw blobs
    for (int i=0; i<blobEngine.count; i++) {
      Blob b = blobEngine.mainArr[i];
      if (b == null || b.free)
        continue;
        
      stroke(b.hue, 255, 255);
      strokeWeight(4);
      noFill();
      ellipse(b.x*scaleX, b.y*scaleY, b.size, b.size);

      noStroke();
      fill(255);
      shadowText(i, b.x*scaleX + 15, b.y*scaleY);      
    }

    // draw bri/sat threshold
    stroke(#FFFFFF);
    strokeWeight(1);
    for (int y=this.marginTop; y < this.marginBottom; y+=4) {
      for (int x=this.marginLeft; x < this.marginRight; x+=4) {
        int i = x + y * this.w;
        int bri = int(brightness(video.pixels[i]));
        int sat = int(saturation(video.pixels[i]));
        if(bri > this.minBri && sat > this.minSat) {
          point(x*scaleX, y*scaleY);
        }
      }
    }
  }
  
}

