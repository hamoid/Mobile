/*
  (c) 2013 code by Abraham Pazos - www.hamoid.com
  Concept and installation by Déborah Borque - www.deborahborque.com
*/

import oscP5.*;
import netP5.*;

class DrawEngine {
  OscP5 osc;
  NetAddress supercollider;
  
  final int SUPERCOLLIDER_OSC_PORT = 57120;
  
  float videoXScale, videoYScale;
  BackgroundOpacityController bgoc;
  String debugString = "";
  
  int motionToSoundSensibility;
  
  void init(Slider s) {
    this.videoXScale = width / float(video.width);
    this.videoYScale = height / float(video.height);
    
    this.osc = new OscP5(this, 12000);
    this.supercollider = new NetAddress("127.0.0.1", SUPERCOLLIDER_OSC_PORT);

    s.add(this, "motionToSoundSensibility", "Sensibilidad del sonido al movimiento", 10, 20, 50, Slider.UP);

    bgoc = new BackgroundOpacityController();
  }

  void draw(boolean debug) {
    if(debug) {
      this.debug();
    } else {
      // Background
      fill(255, bgoc.update());
      rect(0, 0, width, height);
    }

    // lines
    for (int i=0; i<blobEngine.count; i++) {
      Blob b = blobEngine.mainArr[i];
      float vol = 0;
      
      if(!b.free) {
        translate(b.x*this.videoXScale, b.y*this.videoYScale);
        rotate(b.angle); 
        stroke(0, b.alpha);
        strokeWeight(1);
        line(-2500, 0, 2500, 0);
        resetMatrix();
        vol = constrain(map(abs(b.dx), 0, motionToSoundSensibility, 0, 1), 0, 1);
      }
      // make noise
      OscMessage msg = new OscMessage("/erremsg");
      msg.add(i);
      msg.add(vol);
      //msg.add(b.x/float(video.width));
      //msg.add(b.size);
      //msg.add(b.hue);
      this.osc.send(msg, supercollider);
    }
  }
  void debug() {
    // show video frame
    image(video, 0, 0, width, height);
    
    // show hue sat bri (updated every 10 frames)    
    if(frameCount % 10 == 0) {
      color c = get(mouseX, mouseY);
      debugString = 
        "Modo calibración. Pulsa ESPACIO para cambiar la configuración. SHIFT+ENTER para comenzar." + 
        "\nTono:" + int(hue(c)) + 
        "\nSat:" + int(saturation(c)) + 
        "\nBri:" + int(brightness(c));
    }
    fill(0);
    shadowText(debugString, 100, height - 200);
    
    blobEngine.debug(this.videoXScale, this.videoYScale);    
  }
}
class BackgroundOpacityController {
  float start, target, startMs, endMs;
  BackgroundOpacityController() {
    target = 0;
    newTarget(); 
  }
  float update() {
    if(millis() > endMs) {
      newTarget();
    }
    // opacidad del fondo para definir la cantidad de estelas
    // cambiar el segundo numero (entre 230 y 200)
    // 230 = a veces no borra el fondo: superestela :)
    return 230 - 215 * map(millis(), startMs, endMs, start, target);
  }
  void newTarget() {
    start = target;
    target = random(1);
    // if start or target are high, fast transition
    startMs = millis();
    endMs = startMs + 1000 + 30000 * random(1-start) * random(1-target);
  }
}
void shadowText(Object t, float x, float y) {
  fill(#153560);
  text(t.toString(), x, y);
  fill(#FC3575);
  text(t.toString(), x+1, y+1);
}
