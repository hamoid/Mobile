
(c) 2013 code by Abraham Pazos - www.hamoid.com
Concept and installation by Déborah Borque - www.deborahborque.com

# Instalación

## Software

- Descargar Mobile de https://gitlab.com/hamoid/Mobile/
  Junto al botón azúl "Clone" hay un botón de descarga. Selecciona zip.
- Abrir el zip. Eso crea una carpeta Mobile-main. 
- Pon la carpeta donde prefieras en tu disco.
- Cambiale el nombre para que se llame "Mobile" en vez de "Mobile-main".
- Dentro de esa carpeta hay una carpeta vacia llamada "audio".
  Pon archivos en formato wav dentro de la carpeta audio.
  Originalmente teníamos 5 archivos wav de entre 10Mb y 25Mb.
  Asegurate de que los archivo sean todos en estéreo, de 16 bits y de 44100
  Hz.
  
  He puesto 5 ejemplos que puedes descargar en:
  https://hamoid.com/tmp/0.wav
  https://hamoid.com/tmp/1.wav
  https://hamoid.com/tmp/2.wav
  https://hamoid.com/tmp/3.wav
  https://hamoid.com/tmp/4.wav

## Programa para los visuales

- Descargar Processing de https://www.processing.org
  Probado con la versión 4.2. Hay una versión para procesadores M1 y M2.
- Instalar Processing
- Abrir Processing
- Click en Sketch > Import Library... > Manage Libraries...
- Encuentra "Video Library for Processing 4" y haz click en Install
- Encuentra "oscP5" y haz click en Install
- Cierra la ventana "Contribution Manager"

## Programa para el sonido

- Descargar SuperCollider de https://supercollider.github.io/downloads
- Instalar SuperCollider
- Abrir SuperCollider


# Preparativos

- Desactivar Internet para evitar posibles avisos inesperados.
- Asegurarse de que la corriente está enchufada.
- Asegurarse de que no hay temporizadores que apaguen automáticamente el portátil o el proyector.

# Encendido

- Hacer doble click en audio.scd para abrir SuperCollider.
Colocar el cursor en la línea donde pone
"// Haz click en esta línea y pulsa CMD+ENTER"
Pulsar CMD+ENTER para activar el sonido

- Hacer doble click en Mobile.pde para abrir Processing.
Usar la opción "presentar" para activar el programa.

# Calibración

Pulsa la tecla SHIFT para observar lo que ve la webcam.
Así puedes darte cuenta si está usando la webcam correcta.

Comprueba que puedes ver pared de fondo y metacrilato
en el video.

A continuación pulsa la tecla ESPACIO para asignar el margen
izquierdo. Los cuatro márgenes que puedes ajustar sirven para
ignorar lo que hay cerca de los bordes de la pantalla.
Si quieres usar toda la zona disponible puedes poner los margenes
completamente en la izquierda, derecha, arriba y abajo.
La idea de asignar los márgenes es la de reducir al mínimo el
área de detección para que funcione más suave. Especialmente
útiles son el margen superior e inferior, porque sabemos que
los metacrilatos no pueden aparecer en cualquier sitio, solo
a determinadas alturas.

Cuando hayas hecho click para colocar un margen, pulsa ESPACIO
para pasar al siguiente.

Después de asignar los 4 márgenes aparecerán los valores más
importantes para la detección correcta:

Saturación mínima. Si escoges un valor demasiado bajo, todo
se detecta como si fuese metacrilato, incluidas las paredes.
Si escoges un valor demasiado alto, nada se detecta como
metacrilato. Tienes que encontrar el valo adecuado.

Pulsa ESPACO para pasar al brillo mínimo. De nuevo, debes encontrar
el valor que permita distinguir entre pared y metacrilato.
Prueba hacer click y arrastrar hasta encontrar ese valor.

Pulsa ESPACIO para pasar a la "variación de tono máxima".
Este valor especifica cuan estricta debe ser la detección
de color. Si el valor es alto es posible que no distinga
entre los diferentes colores. Si el valor es demasiado bajo,
es posible que un metacrilato aparezca como varios diferentes.

Después de asignar todos los valores puedes volver a repetir
la operación para hare ajustes finos.

Si las condiciones de luz cambian debes hacer los ajustes de nuevo.
Eso puede ocurrir si entra luz natural en el espacio, o si hay
público bloqueando fuentes de luz.

# Crear una aplicación desde Processing

Para no tener que abrir Processing cada vez, con el riesgo de cambiar
el código sin querer, se puede exportar una aplicación.

- Para ello, haz doble click en Mobile.pde para abrir Processing.
- File > Export Application...
- Activa Presentation Mode
- Desactiva Show a Stop button
- Si fuese un Mac nuevo (con procesador M1 en vez de Intel)
  supongo que hay que escoger macOS (Apple Silicon)
- Dale a Export

Tras unos segundos debería haber una carpeta macos-x86_64 con
el programa Mobile en su interior. Así ya no hace falta abrir
Processing. A partir de este momento puedes primero lanzar
SuperCollider, y después abrir el programa Mobile.

# Apagado

- Pulsa ESC para cerrar Mobile
- Si Processing está abierto, cierralo
- Cerrar SuperCollider

# Versions

- 0.0.1 Fades in and out objects
- 0.0.2 Lerps objects
- 0.0.3 Refactored, OOP
- 0.0.4 Slider sets object variables, loads, saves config to disk
- 0.0.5 Indicate motion amount
- 0.0.6 Add margins
- 0.0.7 Add angular speed to blobs, for smoother motion
- 0.0.8 BackgroundOpacityController
- 0.0.9 Bring the project back to life in 2023

# Description

Each blob has a certain color which does not change too much between frames.
We need a list of blobs.
Blobs are areas with high saturation and brightness.

Scan all pixels (step 8 pixels, faster)
Each time I find high sat + bri,
if not already part of a blob,
Flood fill to get blob area (max min x y, find center).
Now part of a new blob.

We have an array of blobs with center, size and hue.

Compare found blobs with previous blob array.

Match Blobs in array by (location, hue) proximity.
Start fading out those not found.
Start fading in new ones

# Requirements

CPU: quad core 2.66 ghz laptop with Intel graphics, 4Gb RAM:
Processing 2.0.1: 25% CPU.
SuperCollider 3.6.3: 1% CPU.

