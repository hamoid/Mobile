/*
  (c) 2013 code by Abraham Pazos - www.hamoid.com
  Concept and installation by Déborah Borque - www.deborahborque.com
*/
class Blob implements Cloneable {
  float x, y, dx, size, angle, dangle;
  int hue, alpha;
  boolean taken, free;
  Blob() {
    init(0, 0, 0, 0);
  }
  void init(int x, int y, int pixelCount, int hue) {
    this.taken = false;
    this.free = false;
    this.alpha = 0;
    this.x = x;
    this.y = y;
    this.dx = 0;
    this.size = int(sqrt(pixelCount));
    this.hue = hue;
    this.angle = random(TWO_PI);
    this.dangle = random(-0.03, 0.03);
  }
  @Override public String toString() { 
    return "x: " + x + " y: " + y + " size: " + size + " hue: " + hue;
  }
  Blob clone() throws CloneNotSupportedException {
    return (Blob) super.clone();
  }
}

